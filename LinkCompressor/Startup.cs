﻿
using LinkCompressor.Middleware;
using Managers.Interface;
using Managers.Manager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Repositories.Interface;
using Repositories.Repository;
using Serilog;
using Serilog.Enrichers;
using Serilog.Events;

namespace LinkCompressor
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.With(new ThreadIdEnricher())
                .Enrich.FromLogContext()
                .WriteTo.RollingFile("./log/log-{Date}.txt", LogEventLevel.Debug, "{ClientIP} {Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting();

            services.AddSingleton(Configuration);
            services.AddTransient<ILinkRepository, LinkRepository>();
            services.AddTransient<ILinkManager, LinkManager>();

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMiddleware<ClientInfoMiddleware>();

            app.UseExceptionHandler();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMvc();

            Log.Information("Запущен");
        }
    }
}
