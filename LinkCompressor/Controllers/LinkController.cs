﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LinkCompressor.Extension;
using Managers.Interface;
using Managers.Objects;
using Microsoft.AspNetCore.Mvc;

namespace LinkCompressor.Controllers
{
    [Route("api/[controller]")]
    public class LinkController : Controller
    {
        private readonly ILinkManager _linkManager;

        public LinkController(ILinkManager linkManager)
        {
            _linkManager = linkManager;
        }


        // GET api/link
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _linkManager.GetAllAsync(HttpContext.GetIp()));
        }

        // POST api/link
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string url)
        {
            string compressUrl = await _linkManager.AddAsync(url, HttpContext.GetIp());

            return Ok(compressUrl);
        }

        // DELETE api/link/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _linkManager.IsExistAsync(id))
            {
                return NotFound(id);
            }

            await _linkManager.DeleteAsync(id);

            return Ok();

        }
    }
}
