﻿using System.Threading.Tasks;
using Managers.Interface;
using Microsoft.AspNetCore.Mvc;

namespace LinkCompressor.Controllers
{
    public class RedirectController: Controller
    {
        private readonly ILinkManager _linkManager;

        public RedirectController(ILinkManager linkManager)
        {
            _linkManager = linkManager;
        }

        [HttpGet("{compressedUrl}")]
        public async Task<IActionResult> RedirectUrl(string compressedUrl)
        {
            string redirectUrl = await _linkManager.GetUrlAndUpdateCountAsync(compressedUrl);

            if (string.IsNullOrEmpty(redirectUrl))
            {
                return new RedirectResult("/", true);
            }

            return new RedirectResult(redirectUrl, true);
        }
    }
}