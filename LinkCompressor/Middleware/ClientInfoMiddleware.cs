﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LinkCompressor.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;

namespace LinkCompressor.Middleware
{
    public class ClientInfoMiddleware
    {
        private RequestDelegate _next;

        public ClientInfoMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var enrichers = new Stack<ILogEventEnricher>();
            enrichers.Push(new PropertyEnricher("ClientIP", context.GetIp()));
        
            using (LogContext.Push(enrichers.ToArray()))
            {
                await _next(context);
            }
        }
    }
}