﻿using System.Net;
using Microsoft.AspNetCore.Http;

namespace LinkCompressor.Extension
{
    public static class HttpContextExtension
    {
        public static string GetIp(this HttpContext ctx)
        {
            if (ctx == null)
            {
                return "localhost";
            }

            return ctx.Connection.RemoteIpAddress.ToString();
        }
    }
}