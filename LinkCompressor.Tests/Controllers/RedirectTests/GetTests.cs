﻿using System.Threading.Tasks;
using LinkCompressor.Controllers;
using Managers.Interface;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;

namespace LinkCompressor.Tests.Controllers.RedirectTests
{

    [TestFixture]
    public class GetTests
    {
        private RedirectController _redirectController;
        private ILinkManager _mockLinkManager;

        [SetUp]
        public void SetUp()
        {
            _mockLinkManager = Substitute.For<ILinkManager>();
            _redirectController = new RedirectController(_mockLinkManager);

            _mockLinkManager.GetUrlAndUpdateCountAsync("gh434").Returns(string.Empty);
            _mockLinkManager.GetUrlAndUpdateCountAsync("gh343").Returns("http://www.yandex.ru");
        }

        [TearDown]
        public void TearDown()
        {
            _redirectController.Dispose();
        }
        
        [Test]
        [TestCase("gh434")]
        public async Task ShouldReturnHomeUrlWhenCompressedUrlNotExist(string shortUrl)
        {
            // Act
            RedirectResult result = (RedirectResult)await _redirectController.RedirectUrl(shortUrl);

            // Assert
            Assert.That(result.Url, Is.EqualTo("/"));
        }

        [Test]
        [TestCase("gh343")]
        public async Task ShouldReturnUrlWhenCompressedUrlExist(string shortUrl)
        {
            // Act
            RedirectResult result = (RedirectResult)await _redirectController.RedirectUrl(shortUrl);

            // Assert
            Assert.That(result.Url, Is.EqualTo("http://www.yandex.ru"));
        }
    }
}