﻿using System.Threading.Tasks;
using LinkCompressor.Controllers;
using Managers.Interface;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;

namespace LinkCompressor.Tests.Controllers.LinkTests
{
    [TestFixture]
    public class DeleteTests
    {
        private LinkController _linkController;
        private ILinkManager _mockLinkManager;

        [SetUp]
        public void SetUp()
        {
            _mockLinkManager = Substitute.For<ILinkManager>();
            _linkController = new LinkController(_mockLinkManager);

            _mockLinkManager.IsExistAsync(5).Returns(false);
            _mockLinkManager.IsExistAsync(7).Returns(true);
        }

        [TearDown]
        public void TearDown()
        {
            _linkController.Dispose();
        }

        [Test]
        [TestCase(5)]
        public async Task ShouldReturnStatusCode404WhenIdNotExist(int id)
        {
            // Act
            NotFoundObjectResult result = (NotFoundObjectResult)await _linkController.DeleteAsync(id);

            // Assert
            Assert.That(result.StatusCode, Is.EqualTo(404));
            Assert.That(result.Value, Is.EqualTo(5));
        }


        [Test]
        [TestCase(7)]
        public async Task ShouldReturnStatusCode200WhenIdExist(int id)
        {
            // Act
            OkResult result = (OkResult)await _linkController.DeleteAsync(id);

            // Assert
            Assert.That(result.StatusCode, Is.EqualTo(200));
        }
    }
}