﻿using System.Linq;
using System.Threading.Tasks;
using LinkCompressor.Controllers;
using Managers;
using Managers.Interface;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;

namespace LinkCompressor.Tests.Controllers.LinkTests
{

    [TestFixture]
    public class PostTests
    {
        private LinkController _linkController;
        private ILinkManager _mockLinkManager;

        [SetUp]
        public void SetUp()
        {
            _mockLinkManager = Substitute.For<ILinkManager>();
            _linkController = new LinkController(_mockLinkManager);

            _mockLinkManager.AddAsync("http://www.yandex.ru", Arg.Any<string>()).Returns("sdhg2ff");
        }

        [TearDown]
        public void TearDown()
        {
            _linkController.Dispose();
        }
        
        [Test]
        [TestCase("http://www.yandex.ru")]
        public async Task ShouldReturnStatusCode200WhenAddNewLink(string url)
        {
            // Act
            OkObjectResult result = (OkObjectResult)await _linkController.Post(url);

            // Assert
            Assert.That(result.StatusCode, Is.EqualTo(200));
            Assert.That(result.Value, Is.EqualTo("sdhg2ff"));
        }
    }
}