﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkCompressor.Controllers;
using Managers.Interface;
using Managers.Objects;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;

namespace LinkCompressor.Tests.Controllers.LinkTests
{
    [TestFixture]
    public class GetTests
    {
        private LinkController _linkController;
        private ILinkManager _mockLinkManager;

        [SetUp]
        public void SetUp()
        {
            _mockLinkManager = Substitute.For<ILinkManager>();
            _linkController = new LinkController(_mockLinkManager);

            _mockLinkManager.GetAllAsync( Arg.Any<string>()).Returns(new List<ApiLink>()
            {
                new ApiLink(),
                new ApiLink(),
            });
        }

        [TearDown]
        public void TearDown()
        {
            _linkController.Dispose();
        }
        
        [Test]
        [TestCase(7)]
        public async Task ShouldReturnStatusCode200WhenManagerReturnLinks(int id)
        {
            // Act
            OkObjectResult result = (OkObjectResult)await _linkController.Get();
            IEnumerable<ApiLink> links = (IEnumerable<ApiLink>) result.Value;

            // Assert
            Assert.That(result.StatusCode, Is.EqualTo(200));
            Assert.That(links.Count(), Is.EqualTo(2));
        }
    }
}