﻿using System.Threading.Tasks;
using Managers;
using Managers.Interface;
using Managers.Manager;
using NSubstitute;
using NUnit.Framework;
using Repositories.Interface;
using Repositories.Objects;

namespace LinkCompressor.Tests.Managers.LinkTests
{
    [TestFixture]
    public class AddTests
    {
        private ILinkManager _linkManager;
        private ILinkRepository _linkRepositoryMock;
        private Compressor _compressor;

        [SetUp]
        public void SetUp()
        {
            _linkRepositoryMock = Substitute.For<ILinkRepository>();
            _linkManager = new LinkManager(_linkRepositoryMock);
            _compressor = new Compressor();

            _linkRepositoryMock
                .GetAsync("LinkId", 0)
                .Returns(Task.FromResult<Link>(null));


            _linkRepositoryMock
                .GetAsync("LinkId", 9)
                .Returns(Task.FromResult(new Link()));
        }

        [Test]
        [TestCase("http://www.yandex.ru","localhost")]
        public async Task ShouldReturnCompressUrlWhenAddNewLink(string url, string ip)
        {
            //Act
            string compressUrl = await _linkManager.AddAsync(url, ip);

            // Assert
            Assert.That(compressUrl, Is.EqualTo(_compressor.GetShortUrl(url)));
        }
    }
}