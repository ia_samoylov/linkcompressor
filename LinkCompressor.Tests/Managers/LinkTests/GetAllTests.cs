﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Components.DictionaryAdapter;
using Managers;
using Managers.Interface;
using Managers.Manager;
using Managers.Objects;
using NSubstitute;
using NUnit.Framework;
using Repositories.Interface;
using Repositories.Objects;

namespace LinkCompressor.Tests.Managers.LinkTests
{
    [TestFixture]
    public class GetAllTests
    {
        private ILinkManager _linkManager;
        private ILinkRepository _linkRepositoryMock;
        private Compressor _compressor;
        private IEnumerable<Link> _items;

        [SetUp]
        public void SetUp()
        {
            _linkRepositoryMock = Substitute.For<ILinkRepository>();
            _linkManager = new LinkManager(_linkRepositoryMock);
            _compressor = new Compressor();

            _items = new List<Link>()
            {
                new Link(){CompressedUrl = "gwr3d", Count = 6, CreateDate = DateTime.UtcNow, OriginalUrl = "http://www.url1.com", LinkId = 12, UserIp = "localhost"},
                new Link(){CompressedUrl = "d2g43", Count = 4, CreateDate = DateTime.UtcNow.AddDays(5), OriginalUrl = "http://www.url5.com", LinkId = 67, UserIp = "localhost"},
            };

            _linkRepositoryMock
                .GetAllAsync(Arg.Any<string>())
                .Returns(Task.FromResult(_items));
        }

        [Test]
        [TestCase("localhost")]
        public async Task ShouldReturnArrayApiLinkWhenReposityryReturnArrayLink(string ip)
        {
            //Act
            IEnumerable<ApiLink> items = await _linkManager.GetAllAsync(ip);
            // Assert

            Assert.That(items.Count, Is.EqualTo(2));
        }
    }
}