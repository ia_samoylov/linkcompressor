﻿using System.Threading.Tasks;
using LinkCompressor.Controllers;
using Managers.Interface;
using Managers.Manager;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using Repositories.Interface;
using Repositories.Objects;

namespace LinkCompressor.Tests.Managers.LinkTests
{
    [TestFixture]
    public class GetUrlAndUpdateCountTests
    {
        private ILinkManager _linkManager;
        private ILinkRepository _linkRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            _linkRepositoryMock = Substitute.For<ILinkRepository>();
            _linkManager = new LinkManager(_linkRepositoryMock);

            _linkRepositoryMock
                .GetAsync("CompressedLink", "notExist")
                .Returns(Task.FromResult<Link>(null));


            _linkRepositoryMock
                .GetAsync("CompressedLink", "exist")
                .Returns(Task.FromResult(new Link()
                {
                    OriginalUrl = "http://www.yandex.ru"
                }));
        }

        [Test]
        [TestCase("notExist")]
        public async Task ShouldReturnNullWhenCompressedUrlNotFound(string shortUrl)
        {
            //Act
            string originalUrl = await _linkManager.GetUrlAndUpdateCountAsync(shortUrl);

            // Assert
            Assert.That(originalUrl, Is.Null);
        }

        [Test]
        [TestCase("exist")]
        public async Task ShouldReturnUrlWhenCompressedUrlFound(string shortUrl)
        {
            //Act
            string originalUrl = await _linkManager.GetUrlAndUpdateCountAsync(shortUrl);

            // Assert
            Assert.That(originalUrl, Is.EqualTo("http://www.yandex.ru"));
        }
    }
}