﻿using System.Threading.Tasks;
using Managers.Interface;
using Managers.Manager;
using NSubstitute;
using NUnit.Framework;
using Repositories.Interface;
using Repositories.Objects;

namespace LinkCompressor.Tests.Managers.LinkTests
{

    [TestFixture]
    public class IsExistTests
    {
        private ILinkManager _linkManager;
        private ILinkRepository _linkRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            _linkRepositoryMock = Substitute.For<ILinkRepository>();
            _linkManager = new LinkManager(_linkRepositoryMock);

            _linkRepositoryMock
                .GetAsync("LinkId", 0)
                .Returns(Task.FromResult<Link>(null));


            _linkRepositoryMock
                .GetAsync("LinkId", 9)
                .Returns(Task.FromResult(new Link()));
        }

        [Test]
        [TestCase(0)]
        public async Task ShouldReturnFalseWhenLinkNotFound(int id)
        {
            //Act
            bool value = await _linkManager.IsExistAsync(id);

            // Assert
            Assert.That(value, Is.False);
        }

        [Test]
        [TestCase(9)]
        public async Task ShouldReturnTrueWhenLinkFound(int id)
        {
            //Act
            bool value = await _linkManager.IsExistAsync(id);

            // Assert
            Assert.That(value, Is.True);
        }
    }
}