# Задача #

Реализовать простой аналог сервиса для сжатия ссылок.

### Реализация на React  ###

* backend: ASP.net CORE
* frontend: reactjs + redux + webpack
* ORM: [Dapper - a simple object mapper for .Net](https://github.com/StackExchange/Dapper)
* СУБД: MSSQL

##### Установка #####
* git clone 
* yarn install
* запустить проект под debug в visual studio 2017
* вписать порт в файле ./webpack/define.js параметра HOST
* yarn run dev

### Реализация на Angular  ###

* backend: ASP.net CORE
* frontend: angular4 + webpack
* ORM: [Dapper - a simple object mapper for .Net](https://github.com/StackExchange/Dapper)
* СУБД: MSSQL

##### Установка #####
* git clone 
* yarn install
* запустить проект под debug в visual studio 2017
* вписать порт в файле ./webpack/define.js параметра HOST
* yarn run dev
