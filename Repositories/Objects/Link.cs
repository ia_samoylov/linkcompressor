﻿using System;
using System.Text;

namespace Repositories.Objects
{
    public class Link
    {
        public int LinkId { get; set; }
        public string UserIp { get; set; }
        public string OriginalUrl { get; set; }
        public string CompressedUrl { get; set; }
        public DateTime CreateDate { get; set; }
        public int Count { get; set; }
    }
}