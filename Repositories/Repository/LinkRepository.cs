﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Repositories.Interface;
using Repositories.Objects;
using Serilog;

namespace Repositories.Repository
{
    public sealed class LinkRepository : ILinkRepository
    {
        private readonly string _connectionString;
        private SqlConnection _connection;

        public LinkRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<IRepository> OpenAsync()
        {
            try
            {
                if (_connection == null || _connection.State != ConnectionState.Open)
                {
                    _connection = new SqlConnection(_connectionString);
                }

                if (_connection.State != ConnectionState.Open)
                {
                    await _connection.OpenAsync();
                }

                return await Task.FromResult(this);
            }
            catch (Exception e)
            {
                throw new Exception("Подключение небыло открыто", e);
            }
        }

        public async Task AddAsync(Link link)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO [dbo].[Link] (Name, Age) VALUES(@Name, @Age);";
                await db.QueryAsync(sqlQuery, link);
            }
        }

        public async Task<IEnumerable<Link>> GetAllAsync(string userIp)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                IEnumerable<Link> links = await db.QueryAsync<Link>("SELECT * FROM [dbo].[Link]");

                return links.ToArray();
            }
        }


        public async Task<Link> GetAsync(string field, object value)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                return await db.QueryFirstAsync<Link>($"SELECT * FROM [dbo].[Link] where [{field}] = @Value", new { Value = value });
            }
        }

        public async Task UpdateAsync(int id, string field, object value)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                await db.ExecuteAsync(
                    $"update [dbo].[Link] set [{field}] = @Value where Link_Id = @Id",
                    new { Id = id, Value = value });
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                await db.ExecuteAsync("DELETE [dbo].[Link] where Link_Id = @Id", new { Id = id });
            }
        }

        public void Dispose()
        {
            if (_connection == null)
                return;

            _connection.Close();
            _connection.Dispose();
            _connection = null;
        }
    }
}