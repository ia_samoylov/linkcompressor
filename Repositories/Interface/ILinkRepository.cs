﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Repositories.Objects;

namespace Repositories.Interface
{
    public interface ILinkRepository:IRepository
    {
        Task AddAsync(Link link);
        Task<IEnumerable<Link>> GetAllAsync(string userIp);
        Task UpdateAsync(int id, string field, object value);
        Task<Link> GetAsync(string field, object value);
        Task DeleteAsync(int id);
    }
}