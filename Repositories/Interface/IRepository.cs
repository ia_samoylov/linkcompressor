﻿using System;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface IRepository : IDisposable
    {
        Task<IRepository> OpenAsync();
    }
}