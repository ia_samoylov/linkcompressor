﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Managers.Interface;
using Managers.Objects;
using Repositories.Interface;
using Repositories.Objects;

namespace Managers.Manager
{
    public sealed class LinkManager : ILinkManager
    {


        private readonly ILinkRepository _linkRepository;
        private readonly Compressor _compressor;

        public LinkManager(ILinkRepository linkRepository)
        {
            _linkRepository = linkRepository;
            _compressor = new Compressor();
        }

        public async Task<IEnumerable<ApiLink>> GetAllAsync(string ip)
        {
            try
            {
                await _linkRepository.OpenAsync();

                IEnumerable<Link> links = await _linkRepository.GetAllAsync(ip);

                return links.Select(l => new ApiLink()
                {
                    LinkId = l.LinkId,
                    OriginalUrl = l.OriginalUrl,
                    CompressedUrl = l.CompressedUrl,
                    CreateDate = l.CreateDate,
                    Count = l.Count
                });
            }
            finally
            {
                _linkRepository.Dispose();
            }
        }

        public async Task<string> AddAsync(string url, string ip)
        {
            try
            {
                await _linkRepository.OpenAsync();

                Link link = new Link
                {
                    UserIp = ip,
                    OriginalUrl = url,
                    CompressedUrl = _compressor.GetShortUrl(url),
                    CreateDate = DateTime.UtcNow
                };

                await _linkRepository.AddAsync(link);

                return link.CompressedUrl;
            }
            finally
            {
                _linkRepository.Dispose();
            }
        }

        public async Task<bool> IsExistAsync(int id)
        {
            try
            {
                await _linkRepository.OpenAsync();
                Link link = await _linkRepository.GetAsync("LinkId", id);

                return link != null;
            }
            finally
            {
                _linkRepository.Dispose();
            }
        }

        public async Task DeleteAsync(int id)
        {
            try
            {
                await _linkRepository.OpenAsync();
                await _linkRepository.DeleteAsync(id);
            }
            finally
            {
                _linkRepository.Dispose();
            }
        }

        public async Task<string> GetUrlAndUpdateCountAsync(string compressedUrl)
        {
            try
            {
                await _linkRepository.OpenAsync();

                Link link = await _linkRepository.GetAsync("CompressedLink", compressedUrl);

                if (link == null)
                {
                    return null;
                }

                await _linkRepository.UpdateAsync(link.LinkId, "Count", ++link.Count);

                return link.OriginalUrl;
            }
            finally
            {
                _linkRepository.Dispose();
            }
        }


    }
}