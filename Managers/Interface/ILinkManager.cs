﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Managers.Objects;
using Repositories.Objects;

namespace Managers.Interface
{
    public interface ILinkManager
    {
        Task<IEnumerable<ApiLink>> GetAllAsync(string ip);
        Task<string> AddAsync(string url, string ip);
        Task<bool> IsExistAsync(int id);
        Task DeleteAsync(int id);
        Task<string> GetUrlAndUpdateCountAsync(string compressedUrl);
    }
}