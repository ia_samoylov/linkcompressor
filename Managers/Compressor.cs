﻿using System;
using System.Linq;

namespace Managers
{
    public class Compressor
    {
        public static readonly string Alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";

        public static readonly int Base = Alphabet.Length;

        public string GetShortUrl(string url)
        {
            var code = 0;

            foreach (var c in url)

            {

                code = (code * Base) + Alphabet.IndexOf(c);
            }

            if (code == 0)
                return Alphabet[0].ToString();

            var s = String.Empty;

            while (code > 0)
            {
                s += Alphabet[code % Base];

                code = code / Base;

            }

            return string.Join(String.Empty, s.Reverse());
        }
    }
}