﻿using System;

namespace Managers.Objects
{
    public class ApiLink
    {
        public int LinkId { get; set; }
        public string OriginalUrl { get; set; }
        public string CompressedUrl { get; set; }
        public DateTime CreateDate { get; set; }
        public int Count { get; set; }
    }
}